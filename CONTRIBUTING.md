# General guidelines

## Coding style (PEP8)

Ensure you follow PEP8 and the style conventions of the surrounding
code as appropriate.

## Ensure all tests are passing

The full set of unit tests must be passing.

## Sort imports

Sort all imports with

    poetry run isort -sl -rc *

## Use public and private naming

Packages, modules and classes can all mark members as "private" by starting
the name with an underscore.

* In a class or module code, defining a symbol as private means it should
  generally not be used outside the defining class/module:

    ```python
    def _foo():
        pass  # Don't use this method outside the current module


    class _Foo:
        pass


    class Public:
        def _private_method():
            pass
    ```

* Modules can be made "package private" by prefixing their name with an
  underscore. This can be used to avoid polluting the "public" namespace
  with lots of names. Anything inside a package private module should not
  be directly referenced outside that module.

  To keep a clean public interface to a package, it is sometimes useful
  to make every module inside a package "private" then import just the
  symbols that you want to be public into `__init__.py`. For example:

    ```python
    # foo/__init__.py
    from ._bar import Bar
    # Bar is now a public symbol from the foo package

    # foo/_bar.py
    # This module is private
    class Bar:
        pass

    # This method is private to the package, since it is inside a private
    # module and is not imported into the __init__.py, but we don't need to
    # have a leading underscore here since the entire module is private
    def some_helper_method()
        pass
    ```

* You can import a private name from a sibling or parent package, but
  not from inside another module (whether that module has a leading
  underscore or not).

    ```python
    from ._bar import Bar  # ok
    from .._foo import Foo  # ok
    from .. import _private  # ok

    from foo import _bar  # not ok
    from .._bar import _xyz  # not ok - xyz is not in
                                # a parent namespace
    import foo._bar  # not ok
    ```

* Exception: test packages can import private names from the module or
  package being tested, (e.g. for the purpose of monkey patching) - even
  if the test code is maintained in a separate package.
