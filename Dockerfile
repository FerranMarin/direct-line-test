FROM python:3.6-slim-buster
RUN pip install fastapi uvicorn
COPY ./simple_api/api /simple_api/api
CMD ["uvicorn", "simple_api.api:app", "--host", "0.0.0.0", "--port", "5000"]