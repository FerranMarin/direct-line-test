#Direct Line Test
##Case Explanation
This code implements a Rest API with only one endpoint that returns the sum of a list of integers.

eg. [1,2,3] => 1+2+3 = 6

The list of numbers is expected to arrive from a backend service and as a test, we will hardcode it as:

    numbers_to_add = list(range(10000001))

The endpoint and example response is as follows:

    Request:
        http://localhost:5000/total/
    
    Response:
    {
        "total": 6
    }

I have additionally add a /docs endpoint to consult the API documentation.

## Development environment

This repository uses [Poetry](https://python-poetry.org/docs/) as its Python depedency manager so:

Install dependencies with

    poetry install

Run the development server with

    poetry run uvicorn simple_api.api:app --reload --port 5000

Navigate to http://localhost:8000/docs to see the API docs and make test
calls.

## Running unit tests

Run the unit tests with

    poetry run pytest --doctest-modules

This will run all unit tests and doctests.

## Verify Style and PEP8

Run flake8 to ensure code is compliant with python style guide:

	poetry run flake8 simple_api

## Building and running docker container

Build the container with

    docker-compose build

Run it with

    docker-compose up

Alternatively you can also run the server forcing a new image build each time with

    docker-compose up --build --force-recreate
