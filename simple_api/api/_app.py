from fastapi import FastAPI

from simple_api.api._router import Router


class App(FastAPI):
    def __init__(self):
        """ Initializes our App and includes the router """
        super().__init__(title='Simple API')
        self.include_router(Router())
