from fastapi import APIRouter
from simple_api.api.models import Body, Response


class Router(APIRouter):
    def __init__(self):
        super().__init__()

        @self.get('/')
        def _healthcheck():
            """
                Purpose: Ping to check API service is up and running \n
                Response:\n
                    "200": {'status': 'ok'}
            """
            return {'status': 'ok'}

        @self.post('/total/', status_code=200, response_model=Response)
        def total_int_list(list_in: Body):
            """
                Purpose: To add together the list of numbers given. \n
                Method: POST \n
                Request: \n
                    {
                        "numbers_to_add: [1, 3, 4, 55 ...]
                    }

                It will accept floats and strings as long as they can be casted
                to integers. \n
                For example: \n
                    "1" will evaluate to 1 \n
                    2.34125 will evaluate to 2 (it does not round)  \n
                    "3.99999" will throw a validation error \n

                Response:\n
                    "200": {
                        "total" : <sum of integers in numbers_to_add list>
                    } \n
                    "422":
                        {"detail":
                            [{
                                "loc": [
                                    "body",
                                    "numbers_to_add",
                                    <index of the non_int castable element>
                                ],
                                "msg": "value is not a valid integer",
                                "type": "type_error.integer"
                            }]
                        } (OR) \n
                        {"detail":
                            [{
                                "loc": ["body", "numbers_to_add"],
                                "msg": "field required",
                                "type": "value_error.missing"
                            }]
                        } \n
                    "405": {"detail":"Method Not Allowed"}
            """
            return {"total": sum(list_in.numbers_to_add)}
