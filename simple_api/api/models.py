from typing import List

from pydantic import BaseModel


class Body(BaseModel):
    """Model which represents a list of integers to add"""

    numbers_to_add: List[int]

    class Config:
        extra = 'forbid'


class Response(BaseModel):
    """ Model which represents the response we desire to give """

    total: int
