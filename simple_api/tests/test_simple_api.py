import unittest
from fastapi.testclient import TestClient

from simple_api.api._app import App


class SimpleApiTestCase(unittest.TestCase):

    def setUp(self) -> None:
        self.client = TestClient(App())
        self.total_endpoint = '/total/'
        self.health_endpoint = '/'

    def test_get_healthcheck(self):
        response = self.client.get(
            f'{self.health_endpoint}'
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'status': 'ok'})

    def test_get_total(self):
        response = self.client.get(
            f'{self.total_endpoint}'
        )
        self.assertEqual(response.status_code, 405)
        self.assertEqual(response.json()['detail'], "Method Not Allowed")

    def test_post_total(self):
        response = self.client.post(
            f'{self.total_endpoint}',
            json={'numbers_to_add': [1, 3, 4]}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['total'], sum([1, 3, 4]))

    def test_post_total_some_strigyfied_int(self):
        response = self.client.post(
            f'{self.total_endpoint}',
            json={'numbers_to_add': [1, "3", 4]}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['total'], sum([1, 3, 4]))

    def test_post_total_all_strigyfied_int(self):
        response = self.client.post(
            f'{self.total_endpoint}',
            json={'numbers_to_add': ["1", "3", "4"]}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['total'], sum([1, 3, 4]))

    def test_post_total_floats(self):
        response = self.client.post(
            f'{self.total_endpoint}',
            json={'numbers_to_add': [1.51, 3.1, 4.0]}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['total'], sum([1, 3, 4]))

    def test_post_total_strigyfied_int_with_floats(self):
        response = self.client.post(
            f'{self.total_endpoint}',
            json={'numbers_to_add': [1, "3", 4.01, "5"]}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['total'], sum([1, 3, 4, 5]))

    def test_post_total_strigified_float(self):
        response = self.client.post(
            f'{self.total_endpoint}',
            json={'numbers_to_add': [1.1123, "3.1", 4.01, "5"]}
        )
        self.assertEqual(response.status_code, 422)
        self.assertEqual(response.json()['detail'][0]['loc'],
                         ['body', 'numbers_to_add', 1])
        self.assertEqual(response.json()['detail'][0]['msg'],
                         'value is not a valid integer')

    def test_post_missing_body(self):
        response = self.client.post(
            f'{self.total_endpoint}'
        )
        self.assertEqual(response.status_code, 422)
        self.assertEqual(response.json()['detail'][0]['loc'],
                         ['body'])
        self.assertEqual(response.json()['detail'][0]['msg'],
                         'field required')

    def test_post_wrong_field(self):
        response = self.client.post(
            f'{self.total_endpoint}',
            json={"foo": "bar"}
        )
        self.assertEqual(response.status_code, 422)
        self.assertEqual(response.json()['detail'][0]['loc'],
                         ['body', 'numbers_to_add'])
        self.assertEqual(response.json()['detail'][0]['msg'],
                         'field required')

    def test_post_extra_field(self):
        response = self.client.post(
            f'{self.total_endpoint}',
            json={"foo": "bar", "numbers_to_add": [1, 3, 4]}
        )
        self.assertEqual(response.status_code, 422)
        self.assertEqual(response.json()['detail'][0]['loc'],
                         ['body', 'foo'])
        self.assertEqual(response.json()['detail'][0]['msg'],
                         'extra fields not permitted')

    def test_post_string(self):
        response = self.client.post(
            f'{self.total_endpoint}',
            json={'numbers_to_add': [1, "foo", 2, "bar"]}
        )
        self.assertEqual(response.status_code, 422)
        self.assertEqual(response.json()['detail'][0]['loc'],
                         ['body', 'numbers_to_add', 1])
        self.assertEqual(response.json()['detail'][0]['msg'],
                         'value is not a valid integer')
        self.assertEqual(response.json()['detail'][1]['loc'],
                         ['body', 'numbers_to_add', 3])
        self.assertEqual(response.json()['detail'][1]['msg'],
                         'value is not a valid integer')

    def test_post_total_example(self):
        numbers_to_add = list(range(10000001))
        response = self.client.post(
            f'{self.total_endpoint}',
            json={'numbers_to_add': numbers_to_add}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['total'], sum(numbers_to_add))
